package com.epam.apigateway.filter;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.filter.GatewayFilter;
import org.springframework.cloud.gateway.filter.factory.AbstractGatewayFilterFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;

import com.epam.apigateway.exception.AuthorizationException;

import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

@Component
@Slf4j
public class AuthenticationFilter extends AbstractGatewayFilterFactory<AuthenticationFilter.Config> {

	@Autowired
	private RouteValidator validator;

	@Autowired
	private WebClient.Builder webClient;

	public AuthenticationFilter() {
		super(Config.class);
	}

	@Override
	public GatewayFilter apply(Config config) {
		return ((exchange, chain) -> {
			if (validator.isSecured.test(exchange.getRequest())) {
				if (!exchange.getRequest().getHeaders().containsKey(HttpHeaders.AUTHORIZATION)) {
					throw new AuthorizationException("missing authorization header");
				}

				String authHeader = exchange.getRequest().getHeaders().get(HttpHeaders.AUTHORIZATION).get(0);
				if (authHeader != null && authHeader.startsWith("Bearer ")) {
					authHeader = authHeader.substring(7);
				}

				log.info("Before proxy, {}", authHeader);
				Mono<ResponseEntity<Void>> responseMono = webClient.build().get()
						.uri("http://identity-service/auth/validate?token=" + authHeader).retrieve().toBodilessEntity()
						.subscribeOn(Schedulers.boundedElastic());

				return responseMono.then(chain.filter(exchange)).onErrorResume(e -> {
					log.info(e.getMessage());
					log.error("invalid access...!");
					throw new AuthorizationException("unauthorized access to application");
				});
			}
			return chain.filter(exchange);
		});
	}

	public static class Config {

	}
}