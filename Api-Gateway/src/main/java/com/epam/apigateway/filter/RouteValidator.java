package com.epam.apigateway.filter;


import java.util.List;
import java.util.function.Predicate;

import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.stereotype.Component;

@Component
public class RouteValidator {
	
	private RouteValidator() {}

    public static final List<String> endPoints = List.of(
            "/gym/trainees/registration",
            "/gym/trainers/registration",
            "/eureka",
            "/auth/token",
            "/auth/validate/**",
            "/auth"
    );

    public Predicate <ServerHttpRequest> isSecured =
            request -> endPoints
                    .stream()
                    .noneMatch(uri -> request.getURI().getPath().contains(uri));
    


}
