package com.epam.gymapplication;


import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import com.epam.gymapplication.controller.LoginController;
import com.epam.gymapplication.service.impl.LoginServiceImpl;


@WebMvcTest(LoginController.class)
class LoginControllerTests {

    @MockBean
    private LoginServiceImpl loginServiceImpl;

    @Autowired
    private MockMvc mockMvc;

 

    @Test
    void testUpdatePassword() throws Exception {
        String userName = "testUser";
        String oldPassword = "oldPassword";
        String newPassword = "newPassword";

        doNothing().when(loginServiceImpl).changePassword(userName, oldPassword, newPassword);

        mockMvc.perform(get("/gym/updatepassword")
                .param("userName", userName)
                .param("oldPassword", oldPassword)
                .param("newPassword", newPassword))
                .andExpect(status().isOk());

        verify(loginServiceImpl).changePassword(userName, oldPassword, newPassword);
    }
}
