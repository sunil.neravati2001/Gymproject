package com.epam.gymapplication;


import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import com.epam.gymapplication.controller.TrainerController;
import com.epam.gymapplication.dto.TrainerDTO;
import com.epam.gymapplication.dto.UpdateTrainerDTO;
import com.epam.gymapplication.dto.response.RegistrationResponse;
import com.epam.gymapplication.dto.response.TrainerProfileResponse;
import com.epam.gymapplication.dto.response.UpdatedTrainerResponse;
import com.epam.gymapplication.service.TrainerService;
import com.fasterxml.jackson.databind.ObjectMapper;

@WebMvcTest(TrainerController.class)
class TrainerControllerTest {
    
    private TrainerProfileResponse trainerProfileResponse;
    
    private RegistrationResponse registrationResponse;
    
    private UpdatedTrainerResponse updatedTrainerResponse;
    
    private TrainerDTO trainerDTO;
    
    private UpdateTrainerDTO updateTrainerDTO;
    
    @MockBean
    private TrainerService trainerService;
    
    @Autowired
    private MockMvc mockMvc;
    
    @Test
    void testAddTrainer() throws Exception {
        
        Mockito.when(trainerService.trainerRegistration(any(TrainerDTO.class))).thenReturn(registrationResponse);

        mockMvc.perform(post("/gym/trainers/registration")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(trainerDTO)))
                .andExpect(status().isCreated());

        Mockito.verify(trainerService, times(1)).trainerRegistration(any(TrainerDTO.class));
    }
    
    @Test
    void testGetTrainerProfileDetails() throws Exception {
        String username = "testUsername";
        
        Mockito.when(trainerService.getTrainerProfileDetails(username)).thenReturn(trainerProfileResponse);

        mockMvc.perform(get("/gym/trainers/profile/{username}", username))
                .andExpect(status().isOk());

        Mockito.verify(trainerService, times(1)).getTrainerProfileDetails(username);
    }
    
    @Test
    void testUpdateProfileDetails() throws Exception {

        Mockito.when(trainerService.updateTrainerDetails(any(UpdateTrainerDTO.class))).thenReturn(updatedTrainerResponse);

        mockMvc.perform(put("/gym/trainers/update")
                .contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(updateTrainerDTO)))
                .andExpect(status().isOk());

        Mockito.verify(trainerService, times(1)).updateTrainerDetails(any(UpdateTrainerDTO.class));
    }
    
    @BeforeEach
    void setUpTrainerProfileResponsex() {
        trainerProfileResponse = new TrainerProfileResponse();
        trainerProfileResponse.setFirstName("34");
        trainerProfileResponse.setLastName("12");
        trainerProfileResponse.setSpecialization("zumba");
        trainerProfileResponse.setIsActive(true);       
    }
    
    @BeforeEach
    void setUpUpdatedTrainerResponse() {
        updatedTrainerResponse = new UpdatedTrainerResponse();
        updatedTrainerResponse.setEmail("1234@gmail.com");
        updatedTrainerResponse.setFirstName("34");
        updatedTrainerResponse.setIsActive(true);
        updatedTrainerResponse.setLastName("12");
        updatedTrainerResponse.setUsername("3412");
        updatedTrainerResponse.setSpecialization("zumba");;
    }
    
    @BeforeEach
    void setUpTrainerDTO() {
        trainerDTO = new TrainerDTO();
        trainerDTO.setEmail("1234@gmail.com");
        trainerDTO.setFirstName("34");
        trainerDTO.setLastName("12");
        trainerDTO.setSpecialization("zumba");;
    }
    
    @BeforeEach
    void setUpRegistrationConverter() {
        registrationResponse = new RegistrationResponse();
        registrationResponse.setUsername("1234");
        registrationResponse.setPassword("1234");
    }

    @BeforeEach
    void setUpUpdatedTrainerDTO() {
        updateTrainerDTO = new UpdateTrainerDTO();
        updateTrainerDTO.setEmail("1234@gmail.com");
        updateTrainerDTO.setFirstName("34");
        updateTrainerDTO.setIsActive(true);
        updateTrainerDTO.setLastName("12");
        updateTrainerDTO.setSpecialization("zumba");
        updateTrainerDTO.setUsername("3412");;
    }

}