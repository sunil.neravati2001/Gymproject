package com.epam.gymapplication;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.util.List;
import java.util.Optional;
import java.util.Set;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.epam.gymapplication.dao.TrainerRepository;
import com.epam.gymapplication.dao.TrainingRepository;
import com.epam.gymapplication.dao.TrainingTypeRepository;
import com.epam.gymapplication.dao.UserRepository;
import com.epam.gymapplication.dto.NotificationDTO;
import com.epam.gymapplication.dto.TrainerDTO;
import com.epam.gymapplication.dto.UpdateTrainerDTO;
import com.epam.gymapplication.dto.response.RegistrationResponse;
import com.epam.gymapplication.dto.response.TraineeResponse;
import com.epam.gymapplication.dto.response.TrainerProfileResponse;
import com.epam.gymapplication.dto.response.UpdatedTrainerResponse;
import com.epam.gymapplication.entity.Trainee;
import com.epam.gymapplication.entity.Trainer;
import com.epam.gymapplication.entity.TrainingType;
import com.epam.gymapplication.entity.User;
import com.epam.gymapplication.exception.TrainerException;
import com.epam.gymapplication.producer.NotificationProducer;
import com.epam.gymapplication.service.impl.TrainerServiceImpl;
import com.epam.gymapplication.utility.CredentialGenerator;
import com.epam.gymapplication.utility.NotificationConverter;
import com.epam.gymapplication.utility.RegistrationConverter;
import com.epam.gymapplication.utility.TraineeConverter;
import com.epam.gymapplication.utility.TrainerConverter;
import com.epam.gymapplication.utility.UserConverter;
import com.fasterxml.jackson.core.JsonProcessingException;

@ExtendWith(MockitoExtension.class)
class TrainerServiceTest {

    private TrainerDTO trainerDTO;

    private User user, user2;

    @Mock
    private UserRepository userRepository;

    @Mock
    private TrainerRepository trainerRepository;

    @Mock
    private TrainingRepository trainingRepository;

    @Mock
    private TrainingTypeRepository trainingTypeRepository;

    @Mock
    private NotificationProducer notificationProducer;

    @Mock
    private CredentialGenerator credentialsGenerator;

    @Mock
    private UserConverter userConverter;

    @Mock
    private NotificationConverter notificationConverter;

    @Mock
    private TraineeConverter traineeConverter;

    @Mock
    private TrainerConverter trainerConverter;

    @Mock
    private PasswordEncoder encoder;

    @Mock
    private RegistrationConverter registrationResponseConverter;

    @InjectMocks
    private TrainerServiceImpl trainerService;

    @BeforeEach
    void setTrainerDTO() {
        trainerDTO = new TrainerDTO("12", "34", "1234@gmail.com", "zumba");
    }

    @BeforeEach
    void setUser() {
        user = new User();
        user.setFirstName("12");
        user.setLastName("34");
        user.setActive(true);
        user.setUsername("1234");
        user.setPassword("1234");
        user.setEmail("1234@gmail.com");
        
        user2 = new User();
        user2.setFirstName("12");
        user2.setLastName("34");
        user2.setActive(true);
        user2.setUsername("1234");
        user2.setPassword("1234");
        user2.setEmail("1234@gmail.com");
    }

    @Test
    void trainerRegistration() throws JsonProcessingException {
        TrainingType trainingType = new TrainingType();
        NotificationDTO notificationDTO = new NotificationDTO();
        RegistrationResponse registrationResponse = new RegistrationResponse();

        Mockito.when(userRepository.findByUsernameStartsWith("1234")).thenReturn(List.of());
        Mockito.when(credentialsGenerator.generateUniqueUsername("12", "34", List.of())).thenReturn("1234");
        Mockito.when(credentialsGenerator.generatePassword()).thenReturn("1234");
        Mockito.when(encoder.encode("1234")).thenReturn("1234");
        Mockito.when(userConverter.getUserFromTrainerDTO(trainerDTO, "1234", "1234")).thenReturn(user);
        Mockito.when(trainingTypeRepository.findByTrainingTypeName("zumba")).thenReturn(Optional.of(trainingType));
        Mockito.when(notificationConverter.getNotificationDTO(user)).thenReturn(notificationDTO);
        Mockito.doNothing().when(notificationProducer).sendNotification(notificationDTO);
        Mockito.when(registrationResponseConverter.getRegistrationResponse("1234", "1234")).thenReturn(registrationResponse);

        assertEquals(registrationResponse, trainerService.trainerRegistration(trainerDTO));

        Mockito.verify(userRepository).findByUsernameStartsWith("1234");
        Mockito.verify(credentialsGenerator).generateUniqueUsername("12", "34", List.of());
        Mockito.verify(credentialsGenerator).generatePassword();
        Mockito.verify(encoder).encode("1234");
        Mockito.verify(userConverter).getUserFromTrainerDTO(trainerDTO, "1234", "1234");
        Mockito.verify(trainingTypeRepository).findByTrainingTypeName("zumba");
        Mockito.verify(notificationConverter).getNotificationDTO(user);
        Mockito.verify(registrationResponseConverter).getRegistrationResponse("1234", "1234");
    }

    @Test
    void trainerRegistrationWithNoTrainingTypeInRepo() throws JsonProcessingException {
        NotificationDTO notificationDTO = new NotificationDTO();
        RegistrationResponse registrationResponse = new RegistrationResponse();

        Mockito.when(userRepository.findByUsernameStartsWith("1234")).thenReturn(List.of());
        Mockito.when(credentialsGenerator.generateUniqueUsername("12", "34", List.of())).thenReturn("1234");
        Mockito.when(credentialsGenerator.generatePassword()).thenReturn("1234");
        Mockito.when(encoder.encode("1234")).thenReturn("1234");
        Mockito.when(userConverter.getUserFromTrainerDTO(trainerDTO, "1234", "1234")).thenReturn(user);
        Mockito.when(notificationConverter.getNotificationDTO(user)).thenReturn(notificationDTO);
        Mockito.doNothing().when(notificationProducer).sendNotification(notificationDTO);
        Mockito.when(registrationResponseConverter.getRegistrationResponse("1234", "1234")).thenReturn(registrationResponse);

        assertEquals(registrationResponse, trainerService.trainerRegistration(trainerDTO));

        Mockito.verify(userRepository).findByUsernameStartsWith("1234");
        Mockito.verify(credentialsGenerator).generateUniqueUsername("12", "34", List.of());
        Mockito.verify(credentialsGenerator).generatePassword();
        Mockito.verify(encoder).encode("1234");
        Mockito.verify(userConverter).getUserFromTrainerDTO(trainerDTO, "1234", "1234");
        Mockito.verify(notificationConverter).getNotificationDTO(user);
    }

    @Test
    void getTrainerProfileWithException() {

        assertThrows(TrainerException.class, () -> trainerService.getTrainerProfileDetails("1"));

    }

    @Test
    void getTrainerProfile() {
        TraineeResponse traineeResponse = new TraineeResponse();
        traineeResponse.setUsername("1234");
        traineeResponse.setFirstName("12");
        traineeResponse.setLastName("34");

        TrainerProfileResponse trainerProfileResponse = new TrainerProfileResponse();
        trainerProfileResponse.setTrainees(List.of(traineeResponse));

        Trainee trainee = new Trainee();
        trainee.setUser(user);

        Trainer trainer = new Trainer();
        trainer.setUser(user2);
        trainer.setTrainees(Set.of(trainee));

        Mockito.when(trainerRepository.findByUserUsername("1234")).thenReturn(Optional.of(trainer));
        Mockito.when(traineeConverter.getTraineeResponse(user)).thenReturn(traineeResponse);
        Mockito.when(trainerConverter.getTrainerProfileResponse(trainer, user2, List.of(traineeResponse)))
                .thenReturn(trainerProfileResponse);

        assertEquals(trainerProfileResponse, trainerService.getTrainerProfileDetails("1234"));

        Mockito.verify(trainerRepository).findByUserUsername("1234");
        Mockito.verify(traineeConverter).getTraineeResponse(user);
        Mockito.verify(trainerConverter).getTrainerProfileResponse(trainer, user2, List.of(traineeResponse));
    }

    @Test
    void updateTrainerDetailsWithException() {

        assertThrows(TrainerException.class, () -> trainerService.updateTrainerDetails(new UpdateTrainerDTO()));

    }

    @Test
    void updateTrainerDetails() throws JsonProcessingException {
        TraineeResponse traineeResponse = new TraineeResponse();
        traineeResponse.setUsername("1234");
        traineeResponse.setFirstName("12");
        traineeResponse.setLastName("34");

        TrainerProfileResponse trainerProfileResponse = new TrainerProfileResponse();
        trainerProfileResponse.setTrainees(List.of(traineeResponse));

        Trainee trainee = new Trainee();
        trainee.setUser(user);

        Trainer trainer = new Trainer();
        trainer.setUser(user2);
        trainer.setTrainees(Set.of(trainee));

        UpdateTrainerDTO updateTrainerDTO = new UpdateTrainerDTO();
        updateTrainerDTO.setUsername("1234");
        updateTrainerDTO.setFirstName("12");
        updateTrainerDTO.setLastName("34");

        UpdatedTrainerResponse updatedTrainerResponse = new UpdatedTrainerResponse();
        updatedTrainerResponse.setUsername("1234");
        updatedTrainerResponse.setFirstName("12");
        updatedTrainerResponse.setLastName("34");
        updatedTrainerResponse.setTrainees(List.of(traineeResponse));

        NotificationDTO notificationDTO = new NotificationDTO();

        Mockito.when(trainerRepository.findByUserUsername("1234")).thenReturn(Optional.of(trainer));
        Mockito.when(userConverter.getUserFromUpdateTrainerDTO(updateTrainerDTO, user2)).thenReturn(user2);
        Mockito.when(traineeConverter.getTraineeResponse(user)).thenReturn(traineeResponse);
        Mockito.when(trainerConverter.getUpdatedResponse(trainer, user2, List.of(traineeResponse)))
                .thenReturn(updatedTrainerResponse);
        Mockito.when(notificationConverter.getNotificationDTO(updatedTrainerResponse)).thenReturn(notificationDTO);
        Mockito.doNothing().when(notificationProducer).sendNotification(notificationDTO);

        assertEquals(updatedTrainerResponse, trainerService.updateTrainerDetails(updateTrainerDTO));

        Mockito.verify(trainerRepository).findByUserUsername("1234");
        Mockito.verify(userConverter).getUserFromUpdateTrainerDTO(updateTrainerDTO, user2);
        Mockito.verify(traineeConverter).getTraineeResponse(user);
        Mockito.verify(trainerConverter).getUpdatedResponse(trainer, user2, List.of(traineeResponse));
        Mockito.verify(notificationConverter).getNotificationDTO(updatedTrainerResponse);
    }


}
