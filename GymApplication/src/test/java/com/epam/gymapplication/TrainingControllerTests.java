package com.epam.gymapplication;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import com.epam.gymapplication.controller.TrainingController;
import com.epam.gymapplication.dto.TrainingDTO;
import com.epam.gymapplication.dto.response.TrainingResponse;
import com.epam.gymapplication.service.TrainingService;
import com.fasterxml.jackson.databind.ObjectMapper;

@WebMvcTest(TrainingController.class)
class TrainingControllerTests {

	@MockBean
	private TrainingService trainingService;

	@Autowired
	private MockMvc mockMvc;

	private ObjectMapper objectMapper;

	private TrainingDTO trainingDto;

	@BeforeEach
	void setUp() {
		objectMapper = new ObjectMapper();
		trainingDto = new TrainingDTO();
		trainingDto.setDuration(2);
		trainingDto.setTraineeUsername("trainee");
		trainingDto.setTrainerUsername("trainer");
		trainingDto.setTrainingName("summer");
		trainingDto.setTrainingType("zumba");
		trainingDto.setTrainingDate(new Date());
	}

	@Test
	void testAddTraining() throws Exception {

		mockMvc.perform(post("/gym/training/add").contentType(MediaType.APPLICATION_JSON)
				.content(objectMapper.writeValueAsString(trainingDto))).andExpect(status().isOk());

	}

	@Test
	void testGetTraineeTrainings() throws Exception {
		String username = "trainee";
		List<TrainingResponse> mockResponse = Collections.emptyList();
		when(trainingService.getTraineeTrainings(eq(username), any(Date.class), any(Date.class), anyString(),
				anyString())).thenReturn(mockResponse);

		mockMvc.perform(get("/gym/training/trainee").param("username", username)).andExpect(status().isOk());

	}

	@Test
	void testGetTrainerTrainings() throws Exception {
		String username = "trainer";
		List<TrainingResponse> mockResponse = Collections.emptyList();
		when(trainingService.getTrainerTrainings(eq(username), any(Date.class), any(Date.class), anyString()))
				.thenReturn(mockResponse);

		mockMvc.perform(get("/gym/training/trainer").param("username", username)).andExpect(status().isOk());

	}
}
