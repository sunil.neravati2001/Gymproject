package com.epam.gymapplication;


import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.kafka.core.KafkaTemplate;

import com.epam.gymapplication.dto.NotificationDTO;
import com.epam.gymapplication.producer.NotificationProducer;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

@ExtendWith(MockitoExtension.class)
class NotificationProducerTest {

    @InjectMocks
    private NotificationProducer notificationProducer;

    @Mock
    private KafkaTemplate<String, String> kafkaTemplate;

    @Mock
    private ObjectMapper objectMapper;

    @Test
    void testSendNotification() throws JsonProcessingException {
        NotificationDTO notificationDTO = new NotificationDTO();
        String expectedJson = "{\"key\": \"value\"}"; 
        when(objectMapper.writeValueAsString(notificationDTO)).thenReturn(expectedJson);
        notificationProducer.sendNotification(notificationDTO);
        notificationProducer.close();
        verify(kafkaTemplate, times(1)).send(any(), any());
    }

    @Test
    void testSendNotificationJsonProcessingError() throws JsonProcessingException {
        NotificationDTO notificationDTO = new NotificationDTO();
        when(objectMapper.writeValueAsString(notificationDTO)).thenThrow(JsonProcessingException.class);
        notificationProducer.close();
        assertThrows(JsonProcessingException.class, () -> notificationProducer.sendNotification(notificationDTO));
    }

    @Test
    void testSendNotificationKafkaError() throws JsonProcessingException {
        NotificationDTO notificationDTO = new NotificationDTO();
        String expectedJson = "{\"key\": \"value\"}"; 
        when(objectMapper.writeValueAsString(notificationDTO)).thenReturn(expectedJson);
        doThrow(new RuntimeException("Test exception")).when(kafkaTemplate).send(anyString(), anyString());
        notificationProducer.close();
        assertDoesNotThrow(() -> notificationProducer.sendNotification(notificationDTO));
    }

    
    @Test
    void testSendNotificationWithNullKafka() throws JsonProcessingException {
        NotificationDTO notificationDTO = new NotificationDTO();
        String expectedJson = "{\"key\": \"value\"}"; 
        when(objectMapper.writeValueAsString(notificationDTO)).thenReturn(expectedJson);
        doThrow(new RuntimeException("Test exception")).when(kafkaTemplate).send(anyString(), anyString());
        kafkaTemplate.destroy();
        notificationProducer.close();
        assertDoesNotThrow(() -> notificationProducer.sendNotification(notificationDTO));
    }
}