package com.epam.gymapplication;


import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.times;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import com.epam.gymapplication.controller.TraineeController;
import com.epam.gymapplication.dto.TraineeDTO;
import com.epam.gymapplication.dto.UpdateTraineeDTO;
import com.epam.gymapplication.dto.response.RegistrationResponse;
import com.epam.gymapplication.dto.response.TraineeProfileResponse;
import com.epam.gymapplication.dto.response.TrainerResponse;
import com.epam.gymapplication.dto.response.UpdatedTraineeResponse;
import com.epam.gymapplication.service.TraineeService;
import com.fasterxml.jackson.databind.ObjectMapper;

@WebMvcTest(TraineeController.class)
class TrianeeControllerTest {

    private TraineeDTO traineeDTO;

    private RegistrationResponse registrationResponse;

    private TraineeProfileResponse traineeProfileResponse;

    private UpdateTraineeDTO updateTraineeDTO;

    private UpdatedTraineeResponse updatedTraineeResponse;

    private TrainerResponse trainerResponse;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private TraineeService traineeService;

    @Test
    void testAddTrainee() throws Exception {
        Mockito.when(traineeService.traineeRegistration(any(TraineeDTO.class))).thenReturn(registrationResponse);

        mockMvc.perform(post("/gym/trainees/registration").contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(traineeDTO))).andExpect(status().isCreated());

        Mockito.verify(traineeService).traineeRegistration(any(TraineeDTO.class));
    }

    @Test
    void testGetTraineeProfileDetails() throws Exception {
        Mockito.when(traineeService.getTraineeProfileDetails("1234")).thenReturn(traineeProfileResponse);
        mockMvc.perform(get("/gym/trainees/profile/{username}", "1234")).andExpect(status().isOk());
        Mockito.verify(traineeService, times(1)).getTraineeProfileDetails("1234");
    }

    @Test
    void testUpdateTraineeProfile() throws Exception {
        Mockito.when(traineeService.updateTraineeDetails(any(UpdateTraineeDTO.class)))
                .thenReturn(updatedTraineeResponse);

        mockMvc.perform(put("/gym/trainees/update").contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(updateTraineeDTO))).andExpect(status().isOk());

        Mockito.verify(traineeService, times(1)).updateTraineeDetails(any(UpdateTraineeDTO.class));
    }
    
    @Test
    void testUpdateInvalidTraineeProfile() throws Exception {
        updateTraineeDTO.setEmail("123");

        mockMvc.perform(put("/gym/trainees/update").contentType(MediaType.APPLICATION_JSON)
                .content(new ObjectMapper().writeValueAsString(updateTraineeDTO))).andExpect(status().isBadRequest());

    }

    @Test
    void testDeleteTrainee() throws Exception {
        String username = "testUsername";

        Mockito.doNothing().when(traineeService).deleteTrainee(username);
        mockMvc.perform(delete("/gym/trainees/delete/{username}", username)).andExpect(status().isOk());

        Mockito.verify(traineeService, times(1)).deleteTrainee(username);
    }

    @Test
    void testNotAssignedTrainers() throws Exception {
        String username = "testUsername";

        List<TrainerResponse> trainerResponses = new ArrayList<>();
        trainerResponses.add(trainerResponse);

        Mockito.when(traineeService.fetchNotAssignedTrainers(username)).thenReturn(trainerResponses);

        mockMvc.perform(get("/gym/trainees/not-assigned-trainers/{username}", username)).andExpect(status().isOk())
                .andExpect(jsonPath("$.size()").value(trainerResponses.size()));

        Mockito.verify(traineeService, times(1)).fetchNotAssignedTrainers(username);
    }

    @Test
    void testUpdateTrainerDetails() throws Exception {
        String username = "testUsername";
        List<String> trainerNames = Arrays.asList("Trainer1");

        List<TrainerResponse> trainerResponses = new ArrayList<>();
        trainerResponses.add(trainerResponse);
        
        Mockito.when(traineeService.updateTrainers(username, trainerNames)).thenReturn(trainerResponses);

        mockMvc.perform(put("/gym/trainees/update/trainer/{username}", username).param("trainerNames",
                trainerNames.toArray(new String[trainerNames.size()]))).andExpect(status().isOk())
                .andExpect(jsonPath("$.size()").value(trainerResponses.size()));

        Mockito.verify(traineeService, times(1)).updateTrainers(username, trainerNames);
    }

    @BeforeEach
    void setUpTraineeDTO() {
        traineeDTO = new TraineeDTO();
        traineeDTO.setAddress("hyd");
        traineeDTO.setEmail("123@gmail.com");
        traineeDTO.setFirstName("12");
        traineeDTO.setLastName("34");
    }

    @BeforeEach
    void setUpRegistrationConverter() {
        registrationResponse = new RegistrationResponse();
        registrationResponse.setUsername("1234");
        registrationResponse.setPassword("1234");
    }

    @BeforeEach
    void setUpTraineeProfileResponse() {
        traineeProfileResponse = new TraineeProfileResponse();
        traineeProfileResponse.setFirstName("12");
        traineeProfileResponse.setLastName("34");
        traineeProfileResponse.setActive(true);
        traineeProfileResponse.setAddress("hyd");
    }

    @BeforeEach
    void setUpUpdateTraineeDTO() {
        updateTraineeDTO = new UpdateTraineeDTO();
        updateTraineeDTO.setAddress("hyd");
        updateTraineeDTO.setEmail("123@gmail.com");
        updateTraineeDTO.setFirstName("12");
        updateTraineeDTO.setLastName("34");
        updateTraineeDTO.setUsername("1234");
        updateTraineeDTO.setIsActive(true);
        updateTraineeDTO.setDateOfBirth(new Date());
    }

    @BeforeEach
    void setUpUpdatedTraineeResponse() {
        updatedTraineeResponse = new UpdatedTraineeResponse();
        updatedTraineeResponse.setActive(true);
        updatedTraineeResponse.setAddress("hyd");
        updatedTraineeResponse.setEmail("123@gmail.com");
        updatedTraineeResponse.setFirstName("12");
        updatedTraineeResponse.setLastName("34");
        updatedTraineeResponse.setTrainers(List.of());
        updatedTraineeResponse.setUsername("1234");
    }

    @BeforeEach
    void setUpTrainerResponse() {
        trainerResponse = new TrainerResponse();
        trainerResponse.setFirstName("34");
        trainerResponse.setLastName("12");
        trainerResponse.setUsername("3412");
        trainerResponse.setSpecialization("zumba");
    }

}