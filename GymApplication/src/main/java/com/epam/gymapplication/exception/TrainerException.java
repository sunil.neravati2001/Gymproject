package com.epam.gymapplication.exception;

public class TrainerException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public TrainerException(String message) {
		super(message);
	}

}
