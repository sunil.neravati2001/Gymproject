package com.epam.gymapplication.exception;

public class TraineeException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public TraineeException(String message) {
		super(message);
	}

}
