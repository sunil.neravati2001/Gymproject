package com.epam.gymapplication.service.impl;

import java.util.List;
import java.util.Set;

import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.epam.gymapplication.dao.TraineeRepository;
import com.epam.gymapplication.dao.TrainerRepository;
import com.epam.gymapplication.dao.TrainingRepository;
import com.epam.gymapplication.dao.UserRepository;
import com.epam.gymapplication.dto.NotificationDTO;
import com.epam.gymapplication.dto.TraineeDTO;
import com.epam.gymapplication.dto.UpdateTraineeDTO;
import com.epam.gymapplication.dto.response.RegistrationResponse;
import com.epam.gymapplication.dto.response.TraineeProfileResponse;
import com.epam.gymapplication.dto.response.TrainerResponse;
import com.epam.gymapplication.dto.response.UpdatedTraineeResponse;
import com.epam.gymapplication.entity.Trainee;
import com.epam.gymapplication.entity.Trainer;
import com.epam.gymapplication.entity.Training;
import com.epam.gymapplication.entity.User;
import com.epam.gymapplication.exception.TraineeException;
import com.epam.gymapplication.exception.TrainerException;
import com.epam.gymapplication.producer.NotificationProducer;
import com.epam.gymapplication.service.TraineeService;
import com.epam.gymapplication.utility.CredentialGenerator;
import com.epam.gymapplication.utility.NotificationConverter;
import com.epam.gymapplication.utility.RegistrationConverter;
import com.epam.gymapplication.utility.TraineeConverter;
import com.epam.gymapplication.utility.TrainerConverter;
import com.epam.gymapplication.utility.UserConverter;
import com.fasterxml.jackson.core.JsonProcessingException;

import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@Transactional
@RequiredArgsConstructor
@Slf4j
public class TraineeServiceImpl implements TraineeService {

	private final UserRepository userRepository;
	private final TraineeRepository traineeRepository;
	private final TrainingRepository trainingRepository;
	private final TrainerRepository trainerRepository;
	private final NotificationProducer notificationProducer;
	private final PasswordEncoder passwordEncoder;
	private final RegistrationConverter registrationConverter;
	private final TraineeConverter traineeConverter;
	private final TrainerConverter trainerConverter;
	private final CredentialGenerator credentialGenerator;
	private final NotificationConverter notificationConverter;
	private final UserConverter userConverter;
	private static final String TRAINEE_EXCEPTION_MESSAGE = "Trainee not found";

	@Override
	public RegistrationResponse traineeRegistration(TraineeDTO traineeDTO) throws JsonProcessingException {

		log.info("Entered trainee Registration method with email {}", traineeDTO.getEmail());

		String name = traineeDTO.getFirstName().toLowerCase() + traineeDTO.getLastName().toLowerCase();

		List<String> usernames = userRepository.findByUsernameStartsWith(name).stream().map(User::getUsername).toList();

		String username = credentialGenerator.generateUniqueUsername(traineeDTO.getFirstName(),
				traineeDTO.getLastName(), usernames);
		String password = credentialGenerator.generatePassword();

		User user = userConverter.getUserFromTraineeDTO(traineeDTO, username, passwordEncoder.encode(password));

		Trainee trainee = new Trainee();
		trainee.setUser(user);
		trainee.setDateOfBirth(traineeDTO.getDateOfBirth());
		trainee.setAddress(traineeDTO.getAddress());

		traineeRepository.save(trainee);

		NotificationDTO notificationDTO = notificationConverter.getNotificationDTO(user);

		notificationProducer.sendNotification(notificationDTO);

		return registrationConverter.getRegistrationResponse(username, password);

	}

	@Override
	public TraineeProfileResponse getTraineeProfileDetails(String username) {

		log.info("Entered get trainee profile details with username {}", username);

		Trainee trainee = traineeRepository.findByUserUsername(username)
				.orElseThrow(() -> new TraineeException(TRAINEE_EXCEPTION_MESSAGE));

		User user = trainee.getUser();

		List<TrainerResponse> trainerProfiles = trainee.getTrainers().stream().map(trainer -> {
			User tempUser = trainer.getUser();
			return trainerConverter.getTrainerResponse(trainer, tempUser);
		}).toList();

		return traineeConverter.getTraineeProfileResponse(trainee, user, trainerProfiles);

	}

	@Override
	public UpdatedTraineeResponse updateTraineeDetails(UpdateTraineeDTO updateTraineeDTO)
			throws JsonProcessingException {

		log.info("Entered updateTraineeDetails with username {}", updateTraineeDTO.getUsername());

		Trainee trainee = traineeRepository.findByUserUsername(updateTraineeDTO.getUsername())
				.orElseThrow(() -> new TraineeException(TRAINEE_EXCEPTION_MESSAGE));

		trainee.setAddress(updateTraineeDTO.getAddress());
		trainee.setDateOfBirth(updateTraineeDTO.getDateOfBirth());

		User user = userConverter.getUserFromUpdateTraineeDTO(updateTraineeDTO, trainee.getUser());
		List<TrainerResponse> trainerProfiles = trainee.getTrainers().stream()
				.map(trainer -> trainerConverter.getTrainerResponse(trainer, trainer.getUser())).toList();

		UpdatedTraineeResponse updatedTraineeResponse = traineeConverter.getUpdatedTraineeResponse(trainee, user,
				trainerProfiles);

		NotificationDTO notificationDTO = notificationConverter.getNotificationDTO(updatedTraineeResponse);

//		Message<NotificationDTO> message = MessageBuilder.withPayload(notificationDTO)
//				.setHeader(KafkaHeaders.TOPIC, NOTIFICATION_TOPIC).build();

		// notificationProxy.sendMail(notificationDTO);
		notificationProducer.sendNotification(notificationDTO);

		return updatedTraineeResponse;

	}

	@Override
	public void deleteTrainee(String username) {

		log.info("Entered delete trainee method with username {}", username);
		Trainee trainee = traineeRepository.findByUserUsername(username)
				.orElseThrow(() -> new TraineeException(TRAINEE_EXCEPTION_MESSAGE));

		trainee.getTrainers().stream().forEach(trainer -> trainer.getTrainees().remove(trainee));

		traineeRepository.delete(trainee);
	}

	@Override
	public List<TrainerResponse> fetchNotAssignedTrainers(String traineeName) {

		log.info("Entered fetchNotAssignedTrainers method with traineeName", traineeName);

		Trainee trainee = traineeRepository.findByUserUsername(traineeName)
				.orElseThrow(() -> new TrainerException("Trainer Not Found"));

		return trainerRepository.findByTraineesNotContaining(trainee).stream()
				.filter(trainer -> trainer.getUser().isActive())
				.map(trainer -> trainerConverter.getTrainerResponse(trainer, trainer.getUser())).toList();
	}

	@Override
	public List<TrainerResponse> updateTrainers(String traineeName, List<String> trainerNames) {

		log.info("Entered update Trainers method for traineee {} ", traineeName);
		Trainee trainee = traineeRepository.findByUserUsername(traineeName)
				.orElseThrow(() -> new TrainerException("Trainee Not Found"));

		Set<Trainer> newTrainers = trainerRepository.findByUserUsernameIn(trainerNames);
		Set<Trainer> oldTrainers = trainee.getTrainers();
		oldTrainers.removeAll(newTrainers);

		oldTrainers.stream().forEach(trainer -> {
			trainer.getTrainees().remove(trainee);
			List<Training> result = trainingRepository.findByTrainerAndTrainee(trainer, trainee);
			trainingRepository.deleteAll(result);
		});

		trainee.setTrainers(newTrainers);

		return newTrainers.stream().map(trainer -> trainerConverter.getTrainerResponse(trainer, trainer.getUser()))
				.toList();

	}

}
