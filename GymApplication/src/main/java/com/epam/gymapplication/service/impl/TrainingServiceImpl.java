package com.epam.gymapplication.service.impl;

import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Service;

import com.epam.gymapplication.dao.TraineeRepository;
import com.epam.gymapplication.dao.TrainerRepository;
import com.epam.gymapplication.dao.TrainingRepository;
import com.epam.gymapplication.dao.UserRepository;
import com.epam.gymapplication.dto.NotificationDTO;
import com.epam.gymapplication.dto.TrainingDTO;
import com.epam.gymapplication.dto.response.TrainingResponse;
import com.epam.gymapplication.entity.Trainee;
import com.epam.gymapplication.entity.Trainer;
import com.epam.gymapplication.entity.User;
import com.epam.gymapplication.exception.TraineeException;
import com.epam.gymapplication.exception.TrainerException;
import com.epam.gymapplication.exception.TrainingException;
import com.epam.gymapplication.exception.UserException;
import com.epam.gymapplication.producer.NotificationProducer;
import com.epam.gymapplication.producer.ReportProducer;
import com.epam.gymapplication.service.TrainingService;
import com.epam.gymapplication.utility.TrainingConverter;
import com.epam.gymapplication.utility.NotificationConverter;
import com.fasterxml.jackson.core.JsonProcessingException;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@RequiredArgsConstructor
@Slf4j
public class TrainingServiceImpl implements TrainingService {

	private final TrainingRepository trainingRepository;
	private final TrainerRepository trainerRepository;
	private final TraineeRepository traineeRepository;
	private final UserRepository userRepository;
	private final NotificationProducer notificationProducer;
	private final ReportProducer reportProducer;
	private final NotificationConverter notificationConverter;
	private final TrainingConverter converter;

	@Override
	public void addTraining(TrainingDTO trainingDTO) throws JsonProcessingException {

		log.info("Entered addTraining with trainee and trainer {} and {}", trainingDTO.getTraineeUsername(),
				trainingDTO.getTrainerUsername());

		Trainee trainee = traineeRepository.findByUserUsername(trainingDTO.getTraineeUsername())
				.orElseThrow(() -> new TraineeException("Trainee Not Found"));

		Trainer trainer = trainerRepository.findByUserUsername(trainingDTO.getTrainerUsername())
				.orElseThrow(() -> new TrainerException("Trainer Not Found"));

		trainingRepository.findByTrainerAndTraineeAndTrainingDate(trainer, trainee, trainingDTO.getTrainingDate())
				.ifPresent(t -> {
					throw new TrainingException(
							"Trainee is already associated with trainer today . Training name :" + t.getTrainingName());
				});

		if (!trainer.getSpecialization().getTrainingTypeName().equalsIgnoreCase(trainingDTO.getTrainingType())) {
			throw new TrainerException("Trainer and training specialization didn't match");
		}

		if (!trainee.getTrainers().contains(trainer)) {
			throw new TrainerException("You haven't assigned to the trainer");
		}

		trainingRepository.save(converter.convertTrainingDTOtoTraining(trainingDTO, trainee, trainer));

		reportProducer.sendReport(converter.getTrainingReportDTO(trainingDTO, trainer));
		NotificationDTO notificationDTO = notificationConverter.getNotificationDTO(trainee, trainer, trainingDTO);
		notificationProducer.sendNotification(notificationDTO);
	}

	@Override
	public List<TrainingResponse> getTraineeTrainings(String username, Date periodFrom, Date periodTo,
			String trainerName, String trainingType) {

		log.info("Entered method getTraineeTrainigs for trainee {}", username);

		User user = userRepository.findByUsername(username).orElseThrow(() -> new UserException("Username not found"));
		return trainingRepository.getTraineeTrainings(user.getUsername(), periodFrom, periodTo, trainerName,
				trainingType);
	}

	@Override
	public List<TrainingResponse> getTrainerTrainings(String username, Date periodFrom, Date periodTo,
			String traineeName) {

		log.info("Entered method getTrainerTrainigs for trainer {}", username);
		User user = userRepository.findByUsername(username).orElseThrow(() -> new UserException("Username not found"));
		return trainingRepository.getTrainerTrainings(user.getUsername(), periodFrom, periodTo, traineeName);
	}

}
