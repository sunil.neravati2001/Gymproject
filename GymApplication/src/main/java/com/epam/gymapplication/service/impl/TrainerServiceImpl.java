package com.epam.gymapplication.service.impl;

import java.util.List;

import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.epam.gymapplication.dao.TrainerRepository;
import com.epam.gymapplication.dao.TrainingTypeRepository;
import com.epam.gymapplication.dao.UserRepository;
import com.epam.gymapplication.dto.NotificationDTO;
import com.epam.gymapplication.dto.TrainerDTO;
import com.epam.gymapplication.dto.UpdateTrainerDTO;
import com.epam.gymapplication.dto.response.RegistrationResponse;
import com.epam.gymapplication.dto.response.TraineeResponse;
import com.epam.gymapplication.dto.response.TrainerProfileResponse;
import com.epam.gymapplication.dto.response.UpdatedTrainerResponse;
import com.epam.gymapplication.entity.Trainer;
import com.epam.gymapplication.entity.TrainingType;
import com.epam.gymapplication.entity.User;
import com.epam.gymapplication.exception.TrainerException;
import com.epam.gymapplication.producer.NotificationProducer;
import com.epam.gymapplication.service.TrainerService;
import com.epam.gymapplication.utility.CredentialGenerator;
import com.epam.gymapplication.utility.NotificationConverter;
import com.epam.gymapplication.utility.RegistrationConverter;
import com.epam.gymapplication.utility.TraineeConverter;
import com.epam.gymapplication.utility.TrainerConverter;
import com.epam.gymapplication.utility.UserConverter;
import com.fasterxml.jackson.core.JsonProcessingException;

import jakarta.transaction.Transactional;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RequiredArgsConstructor
@Service
@Transactional
@Slf4j
public class TrainerServiceImpl implements TrainerService {

	private final TrainerRepository trainerRepository;
	private final UserRepository userRepository;
	private final TrainingTypeRepository trainingTypeRepository;
	private final NotificationProducer notificationProducer;
	private final PasswordEncoder passwordEncoder;
	private final CredentialGenerator credentialGenerator;
	private final UserConverter userConverter;
	private final RegistrationConverter registrationConverter;
	private final TrainerConverter trainerConverter;
	private final TraineeConverter traineeConverter;
	private final NotificationConverter notificationConverter;

	@Override
	public RegistrationResponse trainerRegistration(TrainerDTO trainerDTO) throws JsonProcessingException {

		log.info("Entered trainer Registration method with email {}", trainerDTO.getEmail());
		String name = trainerDTO.getFirstName().toLowerCase() + trainerDTO.getLastName().toLowerCase();
		List<String> usernames = userRepository.findByUsernameStartsWith(name).stream().map(User::getUsername).toList();
		String username = credentialGenerator.generateUniqueUsername(trainerDTO.getFirstName(),
				trainerDTO.getLastName(), usernames);

		String password = credentialGenerator.generatePassword();
		User user = userConverter.getUserFromTrainerDTO(trainerDTO, username, passwordEncoder.encode(password));

		Trainer trainer = new Trainer();
		trainer.setUser(user);

		TrainingType trainingType = trainingTypeRepository.findByTrainingTypeName(trainerDTO.getSpecialization())
				.orElseGet(() -> {
					TrainingType type = new TrainingType();
					type.setTrainingTypeName(trainerDTO.getSpecialization());
					return type;
				});

		trainer.setSpecialization(trainingType);
		trainerRepository.save(trainer);

		NotificationDTO notificationDTO = notificationConverter.getNotificationDTO(user);

		notificationProducer.sendNotification(notificationDTO);

		return registrationConverter.getRegistrationResponse(username, password);

	}

	@Override
	public TrainerProfileResponse getTrainerProfileDetails(String username) {

		log.info("Entered getTrainerProfileDetails for trainer {}", username);
		Trainer trainer = trainerRepository.findByUserUsername(username)
				.orElseThrow(() -> new TrainerException("trainer not found"));

		User user = trainer.getUser();
		List<TraineeResponse> traineeProfiles = trainer.getTrainees().stream()
				.map(trainee -> traineeConverter.getTraineeResponse(trainee.getUser())).toList();
		return trainerConverter.getTrainerProfileResponse(trainer, user, traineeProfiles);

	}

	@Override
	public UpdatedTrainerResponse updateTrainerDetails(UpdateTrainerDTO updateTrainerDTO)
			throws JsonProcessingException {

		log.info("Entered updateTrainerDetails for trainer {}", updateTrainerDTO.getUsername());

		Trainer trainer = trainerRepository.findByUserUsername(updateTrainerDTO.getUsername())
				.orElseThrow(() -> new TrainerException("trainer not found"));

		User user = userConverter.getUserFromUpdateTrainerDTO(updateTrainerDTO, trainer.getUser());
		List<TraineeResponse> traineeProfiles = trainer.getTrainees().stream()
				.map(trainee -> traineeConverter.getTraineeResponse(trainee.getUser())).toList();

		UpdatedTrainerResponse updatedTrainerResponse = trainerConverter.getUpdatedResponse(trainer, user,
				traineeProfiles);

		NotificationDTO notificationDTO = notificationConverter.getNotificationDTO(updatedTrainerResponse);

		notificationProducer.sendNotification(notificationDTO);
		return updatedTrainerResponse;

	}

}
