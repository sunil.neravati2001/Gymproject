package com.epam.gymapplication.service;

import java.util.Date;
import java.util.List;

import com.epam.gymapplication.dto.TrainingDTO;
import com.epam.gymapplication.dto.response.TrainingResponse;
import com.fasterxml.jackson.core.JsonProcessingException;

public interface TrainingService {

	public void addTraining(TrainingDTO trainingDTO) throws JsonProcessingException;
	
	public List<TrainingResponse> getTraineeTrainings(String username, Date periodFrom,Date periodTo,String trainerName, String trainingType);


	public List<TrainingResponse> getTrainerTrainings(String username, Date periodFrom,Date periodTo,String traineeName);

	
}
