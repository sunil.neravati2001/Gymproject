package com.epam.gymapplication.service;

import java.util.List;

import com.epam.gymapplication.dto.TraineeDTO;
import com.epam.gymapplication.dto.UpdateTraineeDTO;
import com.epam.gymapplication.dto.response.RegistrationResponse;
import com.epam.gymapplication.dto.response.TraineeProfileResponse;
import com.epam.gymapplication.dto.response.TrainerResponse;
import com.epam.gymapplication.dto.response.UpdatedTraineeResponse;
import com.fasterxml.jackson.core.JsonProcessingException;

public interface TraineeService {

	RegistrationResponse traineeRegistration(TraineeDTO traineeDTO) throws JsonProcessingException;
	
	TraineeProfileResponse getTraineeProfileDetails(String username);
	
	UpdatedTraineeResponse updateTraineeDetails(UpdateTraineeDTO updateTraineeDTO) throws JsonProcessingException;
	
	void deleteTrainee(String userName);
	
	public List<TrainerResponse> fetchNotAssignedTrainers(String traineeName);
	
	public List<TrainerResponse> updateTrainers(String traineeName,List<String> trainerNames);

}