package com.epam.gymapplication.utility;

import org.springframework.stereotype.Component;

import com.epam.gymapplication.dto.response.RegistrationResponse;

@Component
public class RegistrationConverter {
	
	public RegistrationResponse getRegistrationResponse(String username, String password) {
		RegistrationResponse traineeResponse = new RegistrationResponse();
		traineeResponse.setUsername(username);
		traineeResponse.setPassword(password);
		return traineeResponse;
	}

}
