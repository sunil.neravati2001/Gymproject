package com.epam.gymapplication.utility;

import java.util.List;
import java.util.Map.Entry;

import org.springframework.stereotype.Component;

import com.epam.gymapplication.dto.TrainingDTO;
import com.epam.gymapplication.dto.TrainingReportDTO;
import com.epam.gymapplication.dto.response.ReportResponse;
import com.epam.gymapplication.dto.response.TrainingResponse;
import com.epam.gymapplication.entity.Trainee;
import com.epam.gymapplication.entity.Trainer;
import com.epam.gymapplication.entity.Training;
import com.epam.gymapplication.entity.User;

@Component
public class TrainingConverter {

	private TrainingConverter() {
	}

	public TrainingResponse gettingTrainingResponse(Training training) {
		TrainingResponse trainingResponse = new TrainingResponse();
		trainingResponse.setTrainingDate(training.getTrainingDate());
		trainingResponse.setTrainingDuration(training.getTrainingDuration());
		trainingResponse.setTrainingName(training.getTrainingName());
		trainingResponse.setTrainingType(training.getTrainer().getSpecialization().getTrainingTypeName());
		trainingResponse.setUsername(training.getTrainee().getUser().getUsername());
		return trainingResponse;
	}

	public ReportResponse getReportingResponse(Entry<Trainer, List<Training>> entry, User user) {
		ReportResponse reportResponse = new ReportResponse();
		reportResponse.setActive(user.isActive());
		reportResponse.setFirstName(user.getFirstName());
		reportResponse.setLastName(user.getLastName());
		reportResponse.setUsername(user.getUsername());
		reportResponse.setTrainingResponses(entry.getValue().stream().map(this::gettingTrainingResponse).toList());
		return reportResponse;
	}

	public Training convertTrainingDTOtoTraining(TrainingDTO trainingDTO, Trainee trainee, Trainer trainer) {
		Training training = new Training();
		training.setTrainingDate(trainingDTO.getTrainingDate());
		training.setTrainingDuration(trainingDTO.getDuration());
		training.setTrainingType(trainer.getSpecialization());
		training.setTrainingName(trainingDTO.getTrainingName());
		training.setTrainee(trainee);
		training.setTrainer(trainer);
		return training;
	}

	public TrainingReportDTO getTrainingReportDTO(TrainingDTO trainingDTO, Trainer trainer) {
		TrainingReportDTO trainingReportDTO = new TrainingReportDTO();
		User user = trainer.getUser();
		trainingReportDTO.setFirstName(user.getFirstName());
		trainingReportDTO.setLastName(user.getLastName());
		trainingReportDTO.setUsername(user.getUsername());
		trainingReportDTO.setIsActive(user.isActive());
		trainingReportDTO.setTrainingDate(trainingDTO.getTrainingDate());
		trainingReportDTO.setTrainingDuration(trainingDTO.getDuration());
		trainingReportDTO.setTrainingName(trainingDTO.getTrainingName());
		return trainingReportDTO;
	}

}
