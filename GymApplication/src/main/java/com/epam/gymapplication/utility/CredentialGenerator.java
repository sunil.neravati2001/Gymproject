package com.epam.gymapplication.utility;

import java.security.SecureRandom;
import java.util.List;

import org.springframework.stereotype.Component;

@Component
public class CredentialGenerator {

	private CredentialGenerator() {
	}

	private static final String UPPER_CASE_CHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	private static final String LOWER_CASE_CHARS = "abcdefghijklmnopqrstuvwxyz";
	private static final String DIGITS = "0123456789";
	private static final String SPECIAL_CHARS = "!@#$%^&*()_-+=<>?/{}~";

	private static final String ALL_CHARS = UPPER_CASE_CHARS + LOWER_CASE_CHARS + DIGITS + SPECIAL_CHARS;

	private static SecureRandom secureRandom = new SecureRandom();

	public String generateUniqueUsername(String firstName, String lastName, List<String> userNamesList) {
		String lowerCaseFirstName = firstName.replaceAll("[^a-zA-Z0-9]", "").toLowerCase();
		String lowerCaseLastName = lastName.replaceAll("[^a-zA-Z0-9]", "").toLowerCase();

		String username = lowerCaseFirstName + lowerCaseLastName;
		int suffix = 1;

		if (!userNamesList.contains(username)) {
			return username;
		}
		while (userNamesList.contains(username + suffix)) {
			suffix++;
		}
		username += suffix;
		return username;
	}

	public String generatePassword() {
		StringBuilder password = new StringBuilder();

		for (int i = 0; i < 10; i++) {
			int randomIndex = secureRandom.nextInt(ALL_CHARS.length());
			password.append(ALL_CHARS.charAt(randomIndex));
		}

		return password.toString();
	}
}
