package com.epam.gymapplication.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.epam.gymapplication.dto.TrainerDTO;
import com.epam.gymapplication.dto.UpdateTrainerDTO;
import com.epam.gymapplication.dto.response.RegistrationResponse;
import com.epam.gymapplication.dto.response.TrainerProfileResponse;
import com.epam.gymapplication.dto.response.UpdatedTrainerResponse;
import com.epam.gymapplication.service.TrainerService;
import com.fasterxml.jackson.core.JsonProcessingException;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
@RequestMapping("/gym/trainers")
public class TrainerController {

	private final TrainerService trainerService;

	@PostMapping("/registration")
	public ResponseEntity<RegistrationResponse> addTrainee(@RequestBody @Valid TrainerDTO trainerDTO) throws JsonProcessingException {
		return new ResponseEntity<>(trainerService.trainerRegistration(trainerDTO), HttpStatus.CREATED);
	}

	@GetMapping("/profile/{username}")
	public ResponseEntity<TrainerProfileResponse> trainerProfileDetails(@PathVariable String username) {
		return new ResponseEntity<>(trainerService.getTrainerProfileDetails(username), HttpStatus.OK);
	}

	@PutMapping("/update")
	public ResponseEntity<UpdatedTrainerResponse> updateProfileDetails(
			@RequestBody @Valid UpdateTrainerDTO updateTrainerDTO) throws JsonProcessingException {
		return new ResponseEntity<>(trainerService.updateTrainerDetails(updateTrainerDTO), HttpStatus.OK);
	}
	
}
