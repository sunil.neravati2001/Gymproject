package com.epam.gymapplication.controller;

import java.util.Date;
import java.util.List;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.epam.gymapplication.dto.TrainingDTO;
import com.epam.gymapplication.dto.response.TrainingResponse;
import com.epam.gymapplication.service.TrainingService;
import com.fasterxml.jackson.core.JsonProcessingException;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
@RequestMapping("/gym/training")
public class TrainingController {

	private final TrainingService trainingService;

	@PostMapping("/add")
	public ResponseEntity<Void> addTraining(@RequestBody @Valid TrainingDTO trainingDto) throws JsonProcessingException {

		trainingService.addTraining(trainingDto);
		return new ResponseEntity<>(HttpStatus.OK);
	}

	@GetMapping("/trainee")
	public List<TrainingResponse> getTraineeTrainings(@RequestParam String username,
			@RequestParam(required = false) @DateTimeFormat(pattern = "yyyy-MM-dd") Date periodFrom,
			@RequestParam(required = false) @DateTimeFormat(pattern = "yyyy-MM-dd") Date periodTo,
			@RequestParam(required = false) String trainerName, @RequestParam(required = false) String trainingType) {
		return trainingService.getTraineeTrainings(username, periodFrom, periodTo, trainerName, trainingType);
	}

	@GetMapping("/trainer")
	public List<TrainingResponse> getTrainerTrainings(@RequestParam String username,
			@RequestParam(required = false) @DateTimeFormat(pattern = "yyyy-MM-dd") Date periodFrom,
			@RequestParam(required = false) @DateTimeFormat(pattern = "yyyy-MM-dd") Date periodTo,
			@RequestParam(required = false) String traineeName

	) {
		return trainingService.getTrainerTrainings(username, periodFrom, periodTo, traineeName);
	}
}
