package com.epam.gymapplication.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.epam.gymapplication.dto.TraineeDTO;
import com.epam.gymapplication.dto.UpdateTraineeDTO;
import com.epam.gymapplication.dto.response.RegistrationResponse;
import com.epam.gymapplication.dto.response.TraineeProfileResponse;
import com.epam.gymapplication.dto.response.TrainerResponse;
import com.epam.gymapplication.dto.response.UpdatedTraineeResponse;
import com.epam.gymapplication.service.TraineeService;
import com.fasterxml.jackson.core.JsonProcessingException;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
@RequestMapping("/gym/trainees")
public class TraineeController {

	@Autowired
	private final TraineeService traineeService;

	@PostMapping("/registration")
	public ResponseEntity<RegistrationResponse> addTrainee(@RequestBody @Valid TraineeDTO traineeDTO) throws JsonProcessingException {
		return new ResponseEntity<>(traineeService.traineeRegistration(traineeDTO), HttpStatus.CREATED);
	}

	@GetMapping("/profile/{username}")
	public ResponseEntity<TraineeProfileResponse> traineeProfileDetails(@PathVariable String username) {
		return new ResponseEntity<>(traineeService.getTraineeProfileDetails(username), HttpStatus.OK);
	}

	@PutMapping("/update")
	public ResponseEntity<UpdatedTraineeResponse> updateProfileDetails(
			@RequestBody @Valid UpdateTraineeDTO updateTraineeDTO) throws JsonProcessingException {
		return new ResponseEntity<>(traineeService.updateTraineeDetails(updateTraineeDTO), HttpStatus.OK);
	}

	@DeleteMapping("/delete/{username}")
	public ResponseEntity<Void> deleteTrainee(@PathVariable String username) {
		traineeService.deleteTrainee(username);
		return new ResponseEntity<>(HttpStatus.OK);

	}
	
	@GetMapping("/not-assigned-trainers/{username}")
	public ResponseEntity<List<TrainerResponse>> notAssignedTrainers(@PathVariable String username){
		
		return new ResponseEntity<>(traineeService.fetchNotAssignedTrainers(username),HttpStatus.OK);
	}
	
	@PutMapping("/update/trainer/{username}")
	public ResponseEntity<List<TrainerResponse>> updateTraineeDetails(@PathVariable String username,
			@RequestParam List<String> trainerNames) {

		return new ResponseEntity<>(traineeService.updateTrainers
				(username, trainerNames), HttpStatus.OK);
	}
}
