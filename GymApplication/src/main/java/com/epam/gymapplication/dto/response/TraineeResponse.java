package com.epam.gymapplication.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class TraineeResponse {

	private String firstName;

	private String lastName;

	private String username;
}
