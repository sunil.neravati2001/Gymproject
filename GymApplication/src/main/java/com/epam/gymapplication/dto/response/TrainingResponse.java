package com.epam.gymapplication.dto.response;

import java.util.Date;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class TrainingResponse {

	private String trainingName;
	private Date trainingDate;
	private String trainingType;
	private int trainingDuration;
	private String username;

}
