package com.epam.gymapplication.dto;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import jakarta.validation.constraints.NotBlank;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class TraineeTrainingDTO {

	@NotBlank(message = "username should not be null")
	private String username;

	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date periodFrom;

	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date periodTo;

	private String trainerName;
	
	private String trainingType;

}
