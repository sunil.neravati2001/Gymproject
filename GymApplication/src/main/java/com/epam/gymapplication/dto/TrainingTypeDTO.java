package com.epam.gymapplication.dto;

import jakarta.validation.constraints.Pattern;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@Getter
@Setter
@NoArgsConstructor
public class TrainingTypeDTO {
	
	@Pattern(regexp = "^(?i)(fitness|yoga|Zumba|stretching|resistance)$", message = "Invalid fitness type")
	private String trainingTypeName;

}
