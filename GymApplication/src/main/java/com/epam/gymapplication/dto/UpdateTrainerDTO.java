package com.epam.gymapplication.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.media.Schema.AccessMode;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class UpdateTrainerDTO {

	@NotBlank(message = "FirstName should not be blank")
	private String firstName;

	@NotBlank(message = "LastName should not be blank")
	private String lastName;

	@NotBlank(message = "username should not be blank")
	private String username;

	@Email(message = "email should be valid")
	private String email;

	@Schema(accessMode = AccessMode.READ_ONLY)
	private String specialization;

	@NotNull(message = "Please set the active status")
	private Boolean isActive;

}
