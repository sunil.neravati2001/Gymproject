package com.epam.gymapplication.dto;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import jakarta.validation.constraints.Min;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class TrainingReportDTO {

	@NotBlank(message = "Training name should not be blank")
	private String trainingName;
	
	@Min(value = 1,message = "Enter valid duration")
	private int trainingDuration;
	
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private Date trainingDate;
	
	@NotBlank(message = "username should not be blank")
	private String username;
	
	@NotBlank(message = "FirstName should not be blank")
	private String firstName;
	
	@NotBlank(message = "lastName should not be blank")
	private String lastName;
	
	@NotNull(message = "Please set the active status")
	private Boolean isActive;

}
