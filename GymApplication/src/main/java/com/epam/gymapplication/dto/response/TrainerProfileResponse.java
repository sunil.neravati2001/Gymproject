package com.epam.gymapplication.dto.response;

import java.util.List;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class TrainerProfileResponse {

	private String firstName;

	private String lastName;
	
	private String specialization;
	
	private Boolean isActive;
	
	private List<TraineeResponse> trainees;
}
