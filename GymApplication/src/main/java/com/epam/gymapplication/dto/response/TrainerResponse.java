package com.epam.gymapplication.dto.response;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class TrainerResponse {
	
	private String firstName;
	private String lastName;
	private String username;
	private String specialization;

}
