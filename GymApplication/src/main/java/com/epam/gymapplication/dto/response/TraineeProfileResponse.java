package com.epam.gymapplication.dto.response;

import java.util.Date;
import java.util.List;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class TraineeProfileResponse {

	private String firstName;
	
	private String lastName;

	private Date dateOfBirth;

	private String address;
	
	private boolean isActive;
	
	private List<TrainerResponse> trainers;
	
}
