package com.epam.notificationservice;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import java.util.Collections;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;

import com.epam.notificationservice.dto.NotificationDTO;
import com.epam.notificationservice.dto.NotificationResponse;
import com.epam.notificationservice.dto.Status;
import com.epam.notificationservice.entity.Notification;
import com.epam.notificationservice.entity.NotificationRepository;
import com.epam.notificationservice.service.impl.NotificationServiceImpl;

@ExtendWith(MockitoExtension.class)
class NotificationServiceApplicationTests {

	@Mock
	private JavaMailSender javaMailSender;

	@Mock
	private NotificationRepository notificationRepository;

	@InjectMocks
	private NotificationServiceImpl notificationService;

	private NotificationDTO notificationDTO;

	@BeforeEach
	void setup() {
		notificationDTO = new NotificationDTO();
		notificationDTO.setToEmails(Collections.singletonList("to@example.com"));
		notificationDTO.setCcEmails(Collections.singletonList("cc@example.com"));
		notificationDTO.setEmailType("REGISTRATION");
		notificationDTO.setParameters(Collections.singletonMap("key", "value"));

	}

	@Test
	void testSendNotificationSuccess() {

		doNothing().when(javaMailSender).send(any(SimpleMailMessage.class));
		NotificationResponse response = notificationService.sendNotification(notificationDTO);

		verify(javaMailSender, times(1)).send(any(SimpleMailMessage.class));
		verify(notificationRepository, times(1)).save(any(Notification.class));

		assertEquals(Status.SENT.toString(), response.getStatus());
	}

	@Test
	void testSendNotificationFailure() {

		doThrow(RuntimeException.class).when(javaMailSender).send(any(SimpleMailMessage.class));

		NotificationResponse response = notificationService.sendNotification(notificationDTO);

		verify(javaMailSender, times(1)).send(any(SimpleMailMessage.class));
		verify(notificationRepository, times(1)).save(any(Notification.class));

		assertEquals(Status.FAILED.toString(), response.getStatus());
	}
}
