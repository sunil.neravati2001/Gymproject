package com.epam.notificationservice.dto;

import java.util.List;
import java.util.Map;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class NotificationDTO {

	private List<String> toEmails;

	private List<String> ccEmails;

	private Map<String, String> parameters;

	private String emailType;

	private String body;

	private String subject;

}
