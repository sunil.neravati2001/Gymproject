package com.epam.notificationservice.dto;

public enum Constants {
	REGISTRATION("USER REGISTERED SUCCESSFULLY"),
	UPDATE_TRAINEE("TRAINEE DETAILS UPDATED SUCCESSFULLY"),
	UPDATE_TRAINER("TRAINER DETAILS UPDATED SUCCESSFULLY"),
	TRAINING_REGISTRATION("TRAINING DETAILS ADDED SUCCESSFULLY");

	private final String message;

	Constants(String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}
}
