package com.epam.notificationservice.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.epam.notificationservice.dto.NotificationDTO;
import com.epam.notificationservice.dto.NotificationResponse;
import com.epam.notificationservice.service.NotificationService;

import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
@RequestMapping("/notification")
public class NotificationController {
	
	private final NotificationService notificationService;
	
	@PostMapping("/send")
	public ResponseEntity<NotificationResponse> sendMail(@RequestBody @Valid NotificationDTO notificationDTO){
		return new ResponseEntity<>(notificationService.sendNotification(notificationDTO),HttpStatus.OK);
	}

}
