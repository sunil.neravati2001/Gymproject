package com.epam.notificationservice.listener;


import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import com.epam.notificationservice.dto.NotificationDTO;
import com.epam.notificationservice.service.NotificationService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RequiredArgsConstructor
@Slf4j
@Service
public class Consumer {

	private final NotificationService notificationService;
	private final ObjectMapper objectMapper;

	@KafkaListener(topics = "${notification-topic}", groupId = "${spring.kafka.consumer.group-id}")
	public void sendNotification(String message) throws JsonProcessingException {
		log.info("Entered send notification method, notificationDTO : {}", message);
		NotificationDTO notificationDTO = objectMapper.readValue(message, NotificationDTO.class);
		notificationService.sendNotification(notificationDTO);
	}

}