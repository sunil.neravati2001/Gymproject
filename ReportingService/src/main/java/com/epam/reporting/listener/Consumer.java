package com.epam.reporting.listener;

import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import com.epam.reporting.dto.TrainingReportDTO;
import com.epam.reporting.service.ReportingService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@RequiredArgsConstructor
@Slf4j
public class Consumer {

	public final ReportingService reportingService;

	public final ObjectMapper objectMapper;

	@KafkaListener(topics = "${report-topic}", groupId = "${spring.kafka.consumer.group-id}")
	public void updateReport(String message) throws JsonProcessingException {
		log.info("Entered update report method, trainingReportDTO : {}", message);
		TrainingReportDTO trainingReportDTO = objectMapper.readValue(message, TrainingReportDTO.class);
		reportingService.updateReport(trainingReportDTO);
	}
}
