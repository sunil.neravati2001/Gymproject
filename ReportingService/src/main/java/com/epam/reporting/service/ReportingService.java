package com.epam.reporting.service;

import com.epam.reporting.dto.TrainingReportDTO;
import com.epam.reporting.dto.TrainingSummaryResponse;

public interface ReportingService {
	
	void updateReport(TrainingReportDTO trainingResponse);
	
	TrainingSummaryResponse getReport(String username);

}
