package com.epam.reporting.dto;

import java.util.List;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
public class ReportResponse {
	
	private String firstName;
	private String lastName;
	private String username;
	private boolean isActive;
	private List<TrainingReportDTO> trainingResponses;
	
}
