package com.epam.reporting.dto; 

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public class ExceptionResponse {

	String timeStamp;
	String status;
	String error;
	String path;

}
