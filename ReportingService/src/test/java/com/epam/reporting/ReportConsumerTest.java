package com.epam.reporting;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import com.epam.reporting.dto.TrainingReportDTO;
import com.epam.reporting.listener.Consumer;
import com.epam.reporting.service.ReportingService;
import com.fasterxml.jackson.databind.ObjectMapper;

@ExtendWith(MockitoExtension.class)
class ReportConsumerTest {

    @Mock
    private ReportingService reportingService;

    @Mock
    private ObjectMapper objectMapper;

    @InjectMocks
    private Consumer reportingConsumer;

    @Captor
    private ArgumentCaptor<TrainingReportDTO> trainingCaptor;

    @Test
    void testSendNotification() throws Exception {
        String jsonMessage = "{\"key\": \"value\"}"; 
        TrainingReportDTO expectedReport = new TrainingReportDTO();
        when(objectMapper.readValue(jsonMessage, TrainingReportDTO.class)).thenReturn(expectedReport);

        reportingConsumer.updateReport(jsonMessage);

        verify(reportingService, times(1)).updateReport(trainingCaptor.capture());
        TrainingReportDTO capturedNotification = trainingCaptor.getValue();
        assertEquals(expectedReport, capturedNotification);
    }

}